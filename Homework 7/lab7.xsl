<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
<body style="font-family:Arial;font-size:12pt;background-color:white">

<xsl:for-each select="albumsIlike/album">
  <div style="background-color:lime;color:white;padding:8px">
    <span style="font-weight:bold"><xsl:value-of select="name"/> - </span>
    <xsl:value-of select="artist"/>
    </div>
  <div style="margin-left:20px;margin-bottom:1em;font-size:12pt">
    <p>
    <xsl:value-of select="description"/>
    <span style="font-family:Arial;"> (<xsl:value-of select="date_release"/> is the date of release)</span>
    </p>
  </div>
</xsl:for-each>
</body>
</html>
</xsl:template>

</xsl:stylesheet>
