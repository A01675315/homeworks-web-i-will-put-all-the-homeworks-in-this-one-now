 function password_validation(form)
 {


    if(form.password1.value != "" && form.password1.value == form.password_check.value) // if both fields are completed and identical then :

    {
      if(form.password1.value.length < 8)  // I get the value you entered in the first field, and I check the length
      {
        alert("Password must contain at least 8 characters!");
        return false;
      }
      
      
      re = /[0-9]/;
      if(!re.test(form.password1.value))
       {
        alert("Error: password must contain at least one number (0-9)!");
        return false;
      }
      re = /[a-z]/;
      if(!re.test(form.password1.value)) {
        alert("Error: password must contain at least one lowercase letter (a-z)!");
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(form.password1.value)) {
        alert("Error: password must contain at least one uppercase letter (A-Z)!");
        return false;
      }

    } 

    else {
      alert("Error: Please check that you've entered identicals passwords");
      return false;
    }

    alert("You entered a valid password: " + form.password1.value);
    return true;


  }




