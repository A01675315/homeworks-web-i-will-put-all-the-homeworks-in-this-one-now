<html>

<HEAD>
    <meta charset="utf-8" />
        <link rel="stylesheet" href="style_lab.css" />
<TITLE>

   Lab 9: PHP forms

</TITLE>
</HEAD>
<body>  <p id="titre">Lab 9: PHP forms</p> made by: <p class="pres"> Cyril EQUILBEC A01675315@itesm.mx </p>  </body>



<?php
// define variables and set to empty values
$nameErr = $usernameErr = $genderErr = "";
$name = $username = $gender = $description = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $name = test_input($_POST["name"]);
  $username = test_input($_POST["username"]);
  $description = test_input($_POST["description"]);
  $gender = test_input($_POST["gender"]);
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } else {
    $name = test_input($_POST["name"]);
  }

  if (empty($_POST["username"])) {
    $usernameErr = "Username is required";
  } else {
    $username = test_input($_POST["username"]);
  }


  if (empty($_POST["description"])) {
    $description = "";
  } else {
    $description = test_input($_POST["description"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }
}
?>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

Name: <input type="text" name="name">
<span class="error">* <?php echo $nameErr;?></span>
<br><br>
Username:
<input type="text" name="username">
<span class="error">* <?php echo $usernameErr;?></span>
<br><br>

<br><br>
Write a short description about yourself ! <textarea name="description" rows="3" cols="50"></textarea>
<br><br>
Gender:
<input type="radio" name="gender" value="Male">Male
<input type="radio" name="gender" value="Female">Female
<input type="radio" name="gender" value='Other'> Other
<span class="error">* <?php echo $genderErr;?></span>
<br><br>
<input type="submit" name="submit" value="Submit"> 

</form>

Welcome <?php echo $username;?> ! Your informations are:<br>
<br>

Name: <?php echo $name; ?><br>
Username: <?php echo $username; ?><br>
Your description: <?php echo $description; ?><br>
Gender: <?php echo $gender; ?><br>

<br>
<br>

<ol>
<li>
The view is where data is viewed, usually it's the part where HTML is generated and displayed while the controller handle data that the users submit. That's why we should separate this. 

</li>
<li>
We can use $_REQUEST but it's an older version of Post method.
</li>
<li>
<ul> Functions: 
<li>array_chunk(): divide the array into smallers arrays. </li>
<li> array_diff(): Calculate the difference between 2 arrays. </li>
</ol>
</li>



</ul>


</html>


